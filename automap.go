package automap

type Map[K comparable, V any] struct {
	// threshold determines when to recreate new map to release the memory
	// based on size of mActive and mAll maps
	threshold float32

	// mActive records only the active keys and values
	mActive map[K]V

	// mAll records all the keys it has been set / used
	mAll map[K]bool

	Recreated func(mActive map[K]V)
}

func New[K comparable, V any](threshold float32) *Map[K, V] {
	return &Map[K, V]{
		threshold: threshold,
		mActive:   map[K]V{},
		mAll:      map[K]bool{},
	}
}

func (am *Map[K, V]) Set(key K, value V) {
	am.mActive[key] = value
	am.mAll[key] = true
}

func (am *Map[K, V]) Get(key K) (V, bool) {
	v, ok := am.mActive[key]
	return v, ok
}

func (am *Map[K, V]) Delete(key K) {
	delete(am.mActive, key)

	ratio := float32(len(am.mActive)) / float32(len(am.mAll))
	if ratio < am.threshold {
		am.recreate()
	}
}

func (am *Map[K, V]) ForEach(f func(k K, v V) (done bool)) {
	for k, v := range am.mActive {
		done := f(k, v)
		if done {
			return
		}
	}
}

func (am *Map[K, V]) Size() int {
	return len(am.mActive)
}

func (am *Map[K, V]) recreate() {
	mActive := map[K]V{}
	mAll := map[K]bool{}
	for k, v := range am.mActive {
		mActive[k] = v
		mAll[k] = true
	}
	am.mActive = mActive
	am.mAll = mAll

	if am.Recreated != nil {
		am.Recreated(mActive)
	}
}
