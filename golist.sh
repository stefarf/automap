#!/usr/bin/env bash
VERSION=$1
[ -z "$VERSION" ] && echo "VERSION variable is not defined" && exit 1
go list -m gitlab.com/stefarf/automap@$VERSION
